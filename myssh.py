#!/usr/bin/env python

import base64
import getpass
import os
import socket
import sys
import traceback

import paramiko
#from paramiko.py3compat import raw_input

class myssh:
    def putAll(self, fileName):
        if os.path.isfile(fileName):
            try:
                self.sftp.put(fileName, fileName)
                self.count += 1
            except IOError as e:
                print' %s: %s ' %(e.__class__, e), fileName
        elif os.path.isdir(fileName):
            try:
                self.sftp.mkdir(fileName)
            except IOError as e:
                print 'directory may already exist ', fileName
            for f in os.listdir(fileName):
                self.putAll(fileName + '/' + f)
        else:
            print 'file type not support or exist ', fileName

    def getfilestring(self, fileName):
        try:
            return self.sftp.open(fileName,'r').read()
        except IOError as e:
            print' %s: %s ' %(e.__class__, e), fileName

    def __init__(self, hostname, username = 'insite', password = '2getin', port = 22):
        # setup logging
        paramiko.util.log_to_file('/tmp/demo_sftp.log')

        # get host key, if we know one
        hostkeytype = None
        hostkey = None
        try:
            host_keys = paramiko.util.load_host_keys(os.path.expanduser('~/.ssh/known_hosts'))
        except IOError:
            try:
                # try ~/ssh/ too, because windows can't have a folder named ~/.ssh/
                host_keys = paramiko.util.load_host_keys(os.path.expanduser('~/ssh/known_hosts'))
            except IOError:
                print('*** Unable to open host keys file')
                host_keys = {}

        if hostname in host_keys:
            hostkeytype = host_keys[hostname].keys()[0]
            hostkey = host_keys[hostname][hostkeytype]
            print('Using host key of type %s' % hostkeytype)


        # now, connect and use paramiko Transport to negotiate SSH2 across the connection
        try:
            self.t = paramiko.Transport((hostname, port))
            self.t.connect(username = username, password = password, hostkey = hostkey)
            self.sftp = paramiko.SFTPClient.from_transport(self.t)

        except KeyboardInterrupt as e:
            print('*** Caught exception: %s: %s' %(e.__class__, e))
            #sys.exit(1)
        except Exception as e:
            #print('*** Caught exception: %s: %s' %(e.__class__, e))
            #traceback.print_exc()
            try:
                t.close()
            except:
                pass
            #raise Exception('connect failed')
            raise e

    def __del__(self):
        #print self.count, 'files uploaded'
        if 't' in dir(self):
            self.t.close()

if __name__ == '__main__':


    hostname = '3.35.117.201'

    client = myssh(hostname )
    #file = raw_input('what file to upload:   ')
    #client.putAll(file)

    print client.getfilestring('/tmp/rj')
