import datetime
from django.db import models

# Create your models here.
class Bay(models.Model):
    name = models.CharField(max_length=100)
    ip = models.GenericIPAddressField()

    def __unicode__(self):
        return '%s:%s'%(self.name, self.ip)
### files needs backup
class File(models.Model):
    name = models.CharField(max_length=100)
    route = models.CharField(max_length=400)

    def __unicode__(self):
        return self.name
###
class Md5(models.Model):
    md5 = models.CharField(max_length=100)
    content = models.TextField()

    def __unicode__(self):
        return self.md5[:]

###
class Backuplog(models.Model):
    bay = models.ForeignKey(Bay)
    file = models.ForeignKey(File)
    md5 = models.ForeignKey(Md5)
    date = models.DateTimeField(auto_now = True)

    def __unicode__(self):
        dt = self.date.strftime("%Y-%m-%d %H:%M:%S")# do not want millisecond
        return '%s,%s,%s'  %(self.bay,self.file,dt)
