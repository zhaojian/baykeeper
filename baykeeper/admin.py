from django.contrib import admin

# Register your models here.
from models import Bay,File,Md5,Backuplog

admin.site.register(Bay)
admin.site.register(File)
admin.site.register(Backuplog)
admin.site.register(Md5)
