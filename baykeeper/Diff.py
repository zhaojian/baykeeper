#!/usr/bin/env python

import difflib
import pprint

class Diff:
    def diff2files(self, str1,str2):
        str1 = str1.splitlines()
        str2 = str2.splitlines()
        d = difflib.Differ()
        return list(d.compare(str1,str2))
