from django.shortcuts import render
from baykeeper.models import *
from django.views import generic
import Diff
import BayKeeper
import json

# Create your views here.
from django.http import HttpResponse

def index(request):
    bay_list = Bay.objects.all().order_by('name')
    file_list = File.objects.all().order_by('name')
    context = {'bay_list':bay_list,
            'file_list':file_list,
           # 'ip': request.META['REMOTE_ADDR'],
           }
    request.session['ip'] =  request.META['REMOTE_ADDR']
    return render(request, 'baykeeper/index.html', context)

def bay(request, bay_id):
    bayrc = Bay.objects.get(id = bay_id)
    filtered_log = Backuplog.objects.filter(bay = bayrc)
    context = {'filtered_log':filtered_log,
            'breadcrumbs':bayrc,}
    return render(request, 'baykeeper/filtered_log.html',context)
'''

class BayView(generic.ListView):
    model = Backuplog

    def BayView(self, bay_id):
        self.bay_id = bay_id

    def get_queryset(self):
        bay = Bay.objects.get(id = self.bay_id)
        return Backuplog.objects.filter(bay = bay)
'''
def file(request, file_id):
    filerc = File.objects.get(id = file_id)
    filtered_log = Backuplog.objects.filter(file = filerc)
    context = {'filtered_log': filtered_log,
            'breadcrumbs': filerc,}
    return render(request, 'baykeeper/filtered_log.html', context)

def bklog(request, bklog_id):
    backuplog = Backuplog.objects.get(id = bklog_id)
    md5 = Md5.objects.get(id = backuplog.md5.id)

    context = {'md5':md5,'breadcrumbs':backuplog}
    return render(request, 'baykeeper/md5.html',context)

def diff(request):
    d = Diff.Diff()
    bka = Backuplog.objects.get(id = request.POST['bay_log_a'])
    bkb = Backuplog.objects.get(id = request.POST['bay_log_b'])
    a = Md5.objects.get(id = bka.md5.id)
    b = Md5.objects.get(id = bkb.md5.id)
    diff_list = d.diff2files(a.content,b.content)

    context = {'diff_list' :diff_list, 'breadcrumbs':str(bka) + ' vs ' + str(bkb)}
    return render(request, 'baykeeper/md5.html',context)


def query(request, bay_id):
    bayrc = Bay.objects.get(id = bay_id)
    info = BayKeeper.backuplog(bayrc)
    context = {'backup_response': info}
    return HttpResponse(json.dumps(info), content_type="application/json")
    return render(request, 'baykeeper/query.html', context)




