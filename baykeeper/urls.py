
from django.conf.urls import patterns, url

from baykeeper import views

urlpatterns = [
        url(r'^$', views.index, name = 'index'),
        url(r'^bay(?P<bay_id>[0-9]+)/',views.bay, name = 'bay'),
        url(r'^bklog/(?P<bklog_id>[0-9]+)/', views.bklog, name = 'bklog'),
        url(r'^file(?P<file_id>[0-9]+)/', views.file, name = 'file'),
        url(r'^diff/', views.diff, name = 'diff'),
        url(r'^query/(?P<bay_id>[0-9]+)/', views.query, name = 'query'),
        ]

