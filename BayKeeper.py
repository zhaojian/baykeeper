#!/usr/bin/env python

import traceback
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mysite.settings")
import django
django.setup()
from baykeeper.models import Bay, File, Backuplog, Md5
import hashlib
import myssh
from difflib import *
from pprint import pprint
from multiprocessing import Pool
import sys
from django.core.exceptions import ObjectDoesNotExist

class BayKeeper:

    baylist = Bay.objects.all()
    filelist = File.objects.all()
    backuploglist = Backuplog.objects.all()
    md5list = Md5.objects.all()

    @staticmethod
    def savemd5(content):
        md5 = hashlib.md5(content).hexdigest()
        try:
            md5 = BayKeeper.md5list.get(md5 = md5)
        except ObjectDoesNotExist as e:
            print 'this md5 does not exist in database. save it'
            md5 = Md5(md5 = md5, content = content)
            md5.save()
        return md5

    @staticmethod
    def savelog(bay, file, content):
        md5 = BayKeeper.savemd5(content)
        try:
            log = BayKeeper.backuploglist.filter(bay = bay).filter(file = file).filter(md5 = md5)
            if log:
                print 'this log has already exists'
            else:
                log = Backuplog(bay = bay, file = file, md5 = md5)
                log.save()
                print 'save the log :', log
        except Exception as e:
            print 'except in BayKeeper.savelog: ', e

    @staticmethod
    def backuplog():
        for bay in BayKeeper.baylist:
            try:
                ssh = myssh.myssh(bay.ip)
                for file in BayKeeper.filelist:
                    fl = ssh.getfilestring(file.route)
                    BayKeeper.savelog(bay, file, fl)
            except Exception as e:
                print('*** Caught exception: %s: %s' %(e.__class__, e))
### End if class BayKeeper


def addfile(name, route):
    file = File(name = name, route = route)
    file.save()

def backuplog(bay):
    try:
        ssh = myssh.myssh(bay.ip)
        for file in BayKeeper.filelist:
            fl = ssh.getfilestring(file.route)
            BayKeeper.savelog(bay, file, fl)
    except Exception as e:
        print('*** Caught exception: %s: %s' %(e.__class__, e))
#        traceback.print_exc()

if __name__ == '__main__':
    #addfile('MRconfig.cfg','/w/config/MRconfig.cfg')
    '''
    for i in [Bay, File, Md5, Backuplog]:
        print i
        print i.objects.all()
    '''
    pool = Pool(processes = 10)
    pool.map(backuplog, BayKeeper.baylist)

#print BayKeeper.md5list
#    BayKeeper.backuplog()
    #BayKeeper.testdiff()



